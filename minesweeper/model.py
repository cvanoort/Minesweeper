import numpy as np
from scipy.ndimage import label
from scipy.ndimage.morphology import binary_dilation
from scipy.signal import convolve2d


class MineSweeperModel:
    def __init__(self, shape, mine_frac):
        self.shape = np.array(shape)
        self.mine_frac = mine_frac

        self.mines = self.place_mines(shape=shape, mine_frac=mine_frac)
        self.display_counts = self.get_display_counts()
        self.explore_groups = self.get_explore_groups()
        self.flags = np.zeros(shape, dtype=bool)
        self.explored = np.zeros(shape, dtype=bool)

    def __repr__(self):
        return '{}({}, {})'.format(self.__class__.__name__,
                                   self.shape,
                                   self.mine_frac)

    def explore(self, index):
        if self.mines[index]:
            self.explored[:] = True
            return False

        explore_group = self.explore_groups[index]
        if explore_group:
            inds = self.explore_groups == explore_group
            expanded_inds = binary_dilation(inds, np.ones((3, 3)))
            self.explored[expanded_inds] = True
        else:
            self.explored[index] = True
        return True

    def toggle_flag(self, index):
        self.flags[index] = np.logical_xor(self.flags[index], True)

    def check_win(self):
        if self.field_explored() and self.mines_flagged():
            return True
        else:
            return False

    def check_lose(self):
        return np.any(np.logical_and(self.mines, self.explored))

    @staticmethod
    def place_mines(shape=(10, 10), mine_frac=0.1):
        field = np.zeros(np.prod(shape), dtype=bool)

        mine_count = max(1, int(np.prod(shape) * mine_frac))

        mine_inds = np.random.choice(np.prod(shape), size=mine_count)
        field[mine_inds] = True

        return field.reshape(shape)

    def get_display_counts(self):
        return convolve2d(self.mines, np.ones((3, 3), dtype=int), mode='same')

    def get_explore_groups(self):
        explorables = (self.display_counts == 0)
        structure = np.ones((3, 3), dtype=int)
        explore_groups, _ = label(explorables, structure)
        return explore_groups

    def bound_check(self, index):
        if min(index) < 0:
            return False
        elif np.any([i >= s for s, i in zip(self.shape, index)]):
            return False
        else:
            return True

    def field_explored(self):
        return np.prod(self.shape) == np.sum([self.explored, self.mines])

    def mines_flagged(self):
        return np.all(self.flags == self.mines)
