import argparse

import views
from model import MineSweeperModel


def get_parser():
    parser = argparse.ArgumentParser(
        description="Launches the game Minesweeper.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        '--board_shape',
        type=validate_board_shape,
        default=(20, 20),
        help='Size of the mine field to be explored.',
    )
    parser.add_argument(
        '--mine_frac',
        type=validate_mine_frac,
        default=0.1,
        help='Fraction of field tiles that contain a mine.',
    )
    parser.add_argument(
        '--view',
        type=str.lower,
        default='curses',
        choices=['text', 'curses'],
        help='User interface that processes game interactions.',
    )
    parser.add_argument(
        '--theme',
        type=str.lower,
        default='matrix',
        help='Determines the color theme used by the curses interface.',
    )
    parser.add_argument(
        '-v',
        '--verbose',
        action='count',
        default=0,
        help='Determines the level of terminal output.',
    )

    return parser


class MineSweeperController:
    valid_actions = {'explore', 'move', 'flag', 'reset'}

    def __init__(self, board_shape, mine_frac, view, theme, **kwargs):
        self.shape = board_shape
        self.mine_frac = mine_frac
        self.model = MineSweeperModel(self.shape, self.mine_frac)
        self.game_over = False

        if view == 'curses':
            self.view = views.MineSweeperCursesView(self.model, theme=theme)
        else:
            self.view = views.MineSweeperTextView(self.model)

        self.reset_game()

    def play_minesweeper(self, *args):
        self.game_over = False

        while not self.game_over:
            self.view.refresh()
            self.step_game()

            if self.check_game_over():
                self.finish_game()

                if self.view.check_continue():
                    self.reset_game()

    def step_game(self):
        action, index = self.view.get_next_action()

        if action not in self.valid_actions:
            self.view.invalid_action(action)
        if not self.model.bound_check(index):
            self.view.invalid_index(index)

        if action == 'explore':
            self.model.explore(index)
        elif action == 'flag':
            self.model.toggle_flag(index)
        elif action == 'reset':
            self.reset_game()

    def check_game_over(self):
        return self.model.check_win() or self.model.check_lose()

    def finish_game(self):
        self.view.refresh()
        self.game_over = True

        if self.model.check_lose():
            self.view.show_lose()
        elif self.model.check_win():
            self.view.show_win()

    def reset_game(self):
        self.model = MineSweeperModel(self.shape, self.mine_frac)
        self.view.field = self.model
        self.game_over = False


def validate_board_shape(board_shape):
    board_shape = tuple(int(x.strip()) for x in board_shape.replace(' ', '').split(','))
    if len(board_shape) == 1:
        board_shape = (board_shape[0], board_shape[0])
    assert len(board_shape) == 2
    return board_shape


def validate_mine_frac(mine_frac):
    mine_frac = float(mine_frac)
    assert 0 < mine_frac < 1
    return mine_frac


if __name__ == '__main__':
    args = vars(get_parser().parse_args())
    MineSweeperController(**args).play_minesweeper()
