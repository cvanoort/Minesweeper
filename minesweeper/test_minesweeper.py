import hypothesis.strategies as st
import numpy as np
from hypothesis import given

from model import MineSweeperModel

minefield_given = given(
    shape=st.tuples(
        st.integers(min_value=1, max_value=1000),
        st.integers(min_value=1, max_value=1000)
    ),
    mine_frac=st.floats(
        min_value=0.,
        max_value=1.,
        allow_nan=False,
        allow_infinity=False,
    ),
)


class TestMinefield:
    @minefield_given
    def test_initialization(self, shape, mine_frac):
        minefield = MineSweeperModel(shape=shape, mine_frac=mine_frac)

        assert 1 <= np.sum(minefield.mines) <= np.prod(shape)
        assert np.sum(minefield.flags) == np.sum(minefield.explored) == 0

    def test_get_display_counts(self):
        pass

    @minefield_given
    def test_reveal(self, shape, mine_frac):
        minefield = MineSweeperModel(shape=shape, mine_frac=mine_frac)

        mine_ind = list(zip(*np.where(minefield.mines)))

    def test_get_neighbors(self):
        pass

    def test_toggle_flag(self):
        pass

    def test_check_win(self):
        pass

    def test_field_explored(self):
        pass

    def test_mines_flagged(self):
        pass

    def test_check_lose(self):
        pass
