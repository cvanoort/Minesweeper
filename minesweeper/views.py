import abc
import curses
import os
import time

import numpy as np


def row_2_str(field, row, cursor_loc=None):
    row_str = ''
    for col in range(field.shape[1]):
        if field.flags[row, col]:
            row_str += 'F'
        elif not field.explored[row, col]:
            row_str += '?'
        elif not field.mines[row, col]:
            count = field.display_counts[row, col]
            if count:
                row_str += str(count)
            else:
                row_str += ' '
        else:
            row_str += 'B'

        if cursor_loc and (cursor_loc == (row, col)):
            row_str += '\u0332'
    return row_str


def index_2_str(field, index):
    row, col = index
    if field.flags[row, col]:
        return 'F'
    elif not field.explored[row, col]:
        return '?'
    elif not field.mines[row, col]:
        count = field.display_counts[row, col]
        if count:
            return str(count)
        else:
            return ' '
    else:
        return 'B'


class MineSweeperView(abc.ABC):
    @abc.abstractmethod
    def refresh(self):
        pass

    @abc.abstractmethod
    def get_next_action(self):
        pass

    @abc.abstractmethod
    def invalid_action(self, action, valid_actions=None):
        pass

    @abc.abstractmethod
    def invalid_index(self, index):
        pass

    @abc.abstractmethod
    def show_win(self):
        pass

    @abc.abstractmethod
    def show_lose(self):
        pass

    @abc.abstractmethod
    def check_continue(self):
        pass


class MineSweeperTextView(MineSweeperView):
    def __init__(self, field):
        self.field = field
        self.cursor_loc = (None, None)

        self.terminal_height, self.terminal_width = None, None
        self.refresh()
        self.show_title()
        self.show_info()

    def refresh(self):
        self.check_terminal()
        self.clear_terminal()
        self.show_progress()
        self.show_field()

    def get_next_action(self):
        user_input = input('Your move - ')
        try:
            action, index = user_input.lower().strip().split(':')
            action = action.strip()
            index = tuple(int(x.strip()) for x in index.split(','))
        except ValueError:
            action = user_input.lower().strip()
            index = None

        if index:
            self.cursor_loc = index

        return action, index

    def invalid_action(self, action, valid_actions=None):
        print(f'Action={action} not recognized.')
        if valid_actions:
            print(f'Valid actions are: {valid_actions}.')
        time.sleep(1)

    def invalid_index(self, index):
        print(
            f'{index} is an invalid index, '
            f'(0 to {self.field.shape[0] - 1}, 0 to {self.field.shape[1] - 1}) are valid indices.'
        )
        time.sleep(1)

    def show_win(self):
        print('The minefield is explored, you win!!!')

    def show_lose(self):
        print('You\'ve stepped on a mine, you lose!!!')

    def check_continue(self):
        user_input = input('Continue playing? (y/n): ').lower()

        if 'y' in user_input:
            return True
        elif 'n' in user_input:
            return False
        else:
            print('Input not recognized, please try again.')
            return self.check_continue()

    def show_title(self):
        print("Minesweeper".center(self.terminal_width))
        print(
            "Implemented By: Colin Van Oort".center(self.terminal_width),
            end="\n\n\n",
        )

    def show_info(self):
        print("Explore the minefield and flag all of the mines!".center(self.terminal_width))
        print("You lose if any of the mines are detonated.".center(self.terminal_width), end='\n\n')

        print("Commands (case insensitive):".center(self.terminal_width))
        print("explore: x, y - Reveals the contents of the indicated region".center(self.terminal_width))
        print("flag: x, y - Place a flag on the indicated region.".center(self.terminal_width))
        print("             Note, a flagged region may not be explored".center(self.terminal_width))
        print("             until it is unflagged using the same command.".center(self.terminal_width))
        print("reset - Begins a new game with the same settings.".center(self.terminal_width), end='\n\n')

    def check_terminal(self):
        self.terminal_width, self.terminal_height = os.get_terminal_size()

    def clear_terminal(self):
        print('\n' * self.terminal_height)

    def show_progress(self):
        print(f'Number of Mines: {np.sum(self.field.mines)}')
        print(f'Placed Flags:    {np.sum(self.field.flags)}')

    def show_field(self):
        for row in range(self.field.shape[0]):
            print(row_2_str(self.field, row, cursor_loc=self.cursor_loc).center(self.terminal_width))


class MineSweeperCursesView(MineSweeperView):
    """
    Inspired by https://github.com/cSquaerd/CursaTetra
    """

    def __init__(self, field, theme='matrix'):
        self.field = field
        self.cursor_loc = np.zeros(2, dtype=int)
        self.theme = self.get_theme(theme)

        # Set ESC key delay time
        os.environ.setdefault('ESCDELAY', '25')

        # Initialize screen
        self.main_window = curses.initscr()
        curses.start_color()
        curses.use_default_colors()
        for i in range(curses.COLORS):
            curses.init_pair(i + 1, i, -1)

        curses.noecho()
        curses.cbreak()
        curses.curs_set(0)

        self.title_window = curses.newwin(6, 21, 0, 3)
        self.score_window = curses.newwin(4, 17, 6, 5)
        self.control_window = curses.newwin(7, 23, 10, 2)
        self.board_window = curses.newwin(*(self.field.shape + 2), 0, 27)
        self.board_window.keypad(True)
        self.board_window.nodelay(True)
        self.board_window.move(1, 1)
        self.message_window = curses.newwin(10, 50 + 2, 0, 27 + self.field.shape[1] + 4)

        self.windows = [
            self.title_window,
            self.score_window,
            self.control_window,
            self.board_window,
            self.message_window,
        ]

        # Draw borders on windows
        for window in self.windows:
            window.border()

        self.show_title()
        self.show_controls()
        self.show_score()
        self.message_window.addstr(1, 1, "Game messages:")

        if self.theme:
            self.message_window.addstr(2, 1, f"Using {self.theme.name} theme.")
        else:
            self.message_window.addstr(2, 1, "Using default theme.")

        self.refresh()

    def refresh(self):
        self.show_field()
        self.highlight_cursor()

        for window in self.windows:
            window.refresh()

    def get_next_action(self):
        command, index = 'move', (0, 0)
        action = self.board_window.getch()

        self.lowlight_cursor()

        if action == curses.KEY_LEFT:
            self.cursor_loc += np.array([0, -1], dtype=int)
        elif action == curses.KEY_RIGHT:
            self.cursor_loc += np.array([0, 1], dtype=int)
        elif action == curses.KEY_UP:
            self.cursor_loc += np.array([-1, 0], dtype=int)
        elif action == curses.KEY_DOWN:
            self.cursor_loc += np.array([1, 0], dtype=int)
        elif action == ord('e'):
            command = 'explore'
        elif action == ord('f'):
            command = 'flag'
        elif action == ord('q'):
            del self
            exit()
        elif action == 27:  # ESC or ALT
            n = self.board_window.getch()
            if n == -1:  # Escape was pressed
                del self
                exit()

        # Allow the cursor to wrap around the edges of the board
        self.cursor_loc %= self.field.shape

        return command, tuple(self.cursor_loc)

    def invalid_action(self, action, valid_actions=None):
        self.message_window.addstr(2, 1, f'{action} is an invalid action.')
        if valid_actions:
            self.message_window.addstr(3, 1, f'Valid actions are {valid_actions}.')

    def invalid_index(self, index):
        self.message_window.addstr(2, 1, f'{index} is an invalid index.')

    def show_win(self):
        self.message_window.addstr(2, 1, 'The minefield is explored, you win!!!')

    def show_lose(self):
        self.message_window.addstr(2, 1, 'You\'ve stepped on a mine, you lose!!!')

    def check_continue(self):
        self.message_window.addstr(3, 1, 'Would you like to play again? (y/n)')
        self.message_window.refresh()
        curses.halfdelay(100)
        action = self.board_window.getch()
        if action == ord('y'):
            self.message_window.move(2, 1)
            self.message_window.clrtobot()
            self.message_window.border()
            curses.cbreak()
            return True
        elif action == ord('n'):
            self.message_window.move(2, 1)
            self.message_window.clrtobot()
            self.message_window.border()
            curses.cbreak()
            return False
        else:
            return self.check_continue()

    def show_title(self):
        self.title_window.addstr(1, 4, "Minesweeper")
        self.title_window.addstr(2, 4, 11 * '-')
        self.title_window.addstr(3, 2, "By Colin Van Oort")

    def show_controls(self):
        self.control_window.addstr(1, 6, "CONTROLS:")
        self.control_window.addstr(2, 1, "Arrows: Move cursor")
        self.control_window.addstr(3, 1, "E     : Explore")
        self.control_window.addstr(4, 1, "F     : Flag")
        self.control_window.addstr(5, 1, "ESC/Q : Quit")

    def show_score(self):
        self.score_window.addstr(1, 1, "Explored: 0")
        self.score_window.addstr(2, 1, "Flags:    0")

    def show_field(self):
        if self.theme:
            for row in range(self.field.shape[0]):
                for col in range(self.field.shape[1]):
                    char = index_2_str(self.field, (row, col))
                    self.board_window.addstr(
                        row + 1,
                        col + 1,
                        char,
                        curses.color_pair(self.theme.char2int[char]),
                    )
        else:
            for row in range(self.field.shape[0]):
                self.board_window.addstr(
                    row + 1,
                    1,
                    row_2_str(self.field, row),
                )

    def highlight_cursor(self):
        char = self.board_window.inch(*(self.cursor_loc + 1))
        attrs = char & curses.A_ATTRIBUTES
        self.board_window.chgat(*(self.cursor_loc + 1), 1, attrs | curses.A_STANDOUT)

    def lowlight_cursor(self):
        char = self.board_window.inch(*(self.cursor_loc + 1))
        attrs = char & curses.A_ATTRIBUTES
        self.board_window.chgat(*(self.cursor_loc + 1), 1, attrs ^ curses.A_STANDOUT)

    def __del__(self):
        curses.echo()
        curses.nocbreak()
        curses.endwin()

    @staticmethod
    def get_theme(theme):
        if theme == 'matrix':
            return Matrix()
        else:
            return None


class CursesMinesweeperTheme:
    char2int = {
        '?': 1,
        'F': 2,
        'B': 3,
        '0': 4,
        ' ': 4,
        '1': 5,
        '2': 6,
        '3': 7,
        '4': 8,
        '5': 9,
        '6': 10,
        '7': 11,
        '8': 12,
    }

    def __init__(self):
        pass


class Matrix(CursesMinesweeperTheme):
    char2int = {
        '?': 47,
        'F': 4,
        'B': 2,
        '0': 0,
        ' ': 0,
        '1': 29,
        '2': 119,
        '3': 185,
        '4': 227,
        '5': 209,
        '6': 203,
        '7': 125,
        '8': 197,
    }

    def __init__(self):
        super().__init__()
        self.name = 'Matrix'


# Use this to view the default color pairs
# Borrowed from:
#     https://stackoverflow.com/questions/18551558/how-to-use-terminal-color-palette-with-curses
if __name__ == '__main__':
    def main(stdscr):
        curses.start_color()
        curses.use_default_colors()
        for i in range(curses.COLORS):
            curses.init_pair(i + 1, i, -1)
        try:
            for i in range(0, 255):
                stdscr.addstr(str(i), curses.color_pair(i))
        except curses.ERR:
            # End of screen reached
            pass
        stdscr.getch()


    curses.wrapper(main)
